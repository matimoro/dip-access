package pl.multivisionit.oais.dipaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.multivisionit.oais.dipaccess.model.Dataset;

@Repository
public interface DatasetRepository extends JpaRepository<Dataset, Long> {

}
