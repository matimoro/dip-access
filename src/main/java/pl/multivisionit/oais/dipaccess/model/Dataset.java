package pl.multivisionit.oais.dipaccess.model;

import lombok.Data;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "dataset")
public class Dataset {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    @Column(name="value")
    private int value;
    @Column(name="create_date")
    private LocalDateTime createDate;
    @Column(name="update_date")
    private LocalDateTime updateDate;
    @Column(name="device_id")
    private int deviceId;

}
