package pl.multivisionit.oais.dipaccess.controller.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.multivisionit.oais.dipaccess.model.Dataset;
import pl.multivisionit.oais.dipaccess.repository.DatasetRepository;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/dataset-management", produces = { MediaType.APPLICATION_JSON_VALUE })
public class DatasetRESTController {

    @Autowired
    DatasetRepository datasetRepository;

    @GetMapping(value = "/datasets")
    public List<Dataset> getAllDatasets() {
        log.debug("DatasetRESTController getAllDatasets method execution");
        return datasetRepository.findAll();
    }
}
